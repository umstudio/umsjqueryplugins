# UMS One Slider

Plugin de slider responsivo.

![Alt UMS One Slider](http://www.umstudiohomolog.com.br/ums/jquery-plugins/ums-one-slider/preview.jpg)

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Estrutura Básica (HTML)
Criar uma div com a classe **.one-slider-wrapper**.

```html
<link rel="stylesheet" href="css/jquery.ums-one-slider.css">
```
```html
<div class="one-slider-wrapper">
    <ul>
        <li>SLIDE 1</li>
        <li>SLIDE 2</li>
        <li>SLIDE 3</li>
        <li>SLIDE 4</li>
        <li>SLIDE 5</li>
    </ul>
</div>
```
```html
<script src="js/jquery.ums-one-slider.js"></script>
```
### Chamada Plugin

```js
$('.container-client ul').jqueryUmsOneSlider({
	'loop' : true, //true/false
	'pagination' : true, //true/false
	'navigation' : true, //true/false
	'caption' : true, //true/false
	'autoplay' : true, //true/false
	'velocity' : 1000, //ex: 1000 = 1s
	'interval' : 3000, //ex: 3000 = 3s
	'easing' : 'easeInOutCirc',
	'onButtonClick' : function(p_button){},
	'onNextButtonClick': function(){},
	'onPrevButtonClick': function(){}
});
```

### Métodos
```js
var objSlider = new $('.container-client ul').jqueryUmsOneSlider();

```
##### Play
Ínicia a navegação do slider automático.
```js
objSlider.jqueryUmsOneSlider('play');
```
#### Pause
Pausa a navegação.
```js
objSlider.jqueryUmsOneSlider('pause');
```
#### Prev
Volta a navegação do slider.
```js
objSlider.jqueryUmsOneSlider('prev');
```
#### Next
Avança a navegação do slider.
```js
objSlider.jqueryUmsOneSlider('next');
```
#### Goto
Navega para a posição desejada do slider.
```js
objSlider.jqueryUmsOneSlider('goto', 4);
```
#### Reset
Reseta o slider para posição inicial.
```js
objSlider.jqueryUmsOneSlider('reset');
```
#### Page
Retorna o slider atual.
```js
objSlider.jqueryUmsOneSlider('page');
```

#### Opções para o Easing
    - easeInSine
    - easeOutSine
    - easeInOutSine
    - easeInQuad
    - easeOutQuad
    - easeInOutQuad
    - easeInCubic
    - easeOutCubic
    - easeInOutCubic
    - easeInQuart
    - easeOutQuart
    - easeInOutQuart
    - easeInQuint
    - easeOutQuint
    - easeInOutQuint
    - easeInExpo
    - easeOutExpo
    - easeInOutExpo
    - easeInCirc
    - easeOutCirc
    - easeInOutCirc
    - easeInBack
    - easeOutBack
    - easeInOutBack
    - easeInElastic
    - easeOutElastic
    - easeInOutElastic
    - easeInBounce
    - easeOutBounce
    - easeInOutBounce
Exemplos em: [EASINGS.NET]

### Dependências
* [Velocity]*
* [jQuery Mobile Events]*

~~~
*Já Incluida no plugin
~~~
[Velocity]:http://julian.com/research/velocity/
[jQuery Mobile Events]:https://github.com/benmajor/jQuery-Touch-Events
[easings.net]:http://http://easings.net/pt