var umsapp = umsapp || {};
umsapp.Tmain = function($, objname, options)
{
	'use strict';
	var self = this;

	this.init = function()
	{
		self.initVars();
		self.processTriggers();
		self.onElementsEvents();
	};

	this.initVars = function()
	{
		
	};

	this.onReady = function()
	{
		// CODE ON APLICATION IS READY
		self.start();
	};

	this.start = function()
	{
		// CODE ON APLICATION IS STARTED
		self.triggerStarted();
	};

	this.processTriggers = function()
	{

	};

	this.onElementsEvents = function()
	{

	};

	CjsBaseClass.call(this, $, objname, options);

	this.init();
};

umsapp.main = new umsapp.Tmain
(
	window.cjsbaseclass_jquery,
	'main',
	{
		'debug': CJS_DEBUG_MODE_1,
		'highlighted': 'auto'
	}
);