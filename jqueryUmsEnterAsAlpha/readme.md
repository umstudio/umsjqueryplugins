# UMS Enter as Alpha
> Plugin para exibir elementos no site dee acordo com o window scroll.

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Demo
<http://umstudiohomolog.com.br/ums/jquery-plugins/jqueryUmsEnterAsAlpha/demo/>

### Estrutura Básica (HTML)
```html
<script src="js/cjsbaseclass.min.js"></script>
<script src="js/ums_enter_as_alpha.min.js" data-trigger_show="25"></script>
```
### Dependências
* [cjsbaseclass]

[cjsbaseclass]:https://www.npmjs.com/package/cjsbaseclass