var umsplugin_enter_alpha = umsplugin_enter_alpha || {};

umsplugin_enter_alpha.options = {};
umsplugin_enter_alpha.scripts = document.getElementsByTagName('script');
umsplugin_enter_alpha.current_script = umsplugin_enter_alpha.scripts[umsplugin_enter_alpha.scripts.length - 1];
umsplugin_enter_alpha.script_name = umsplugin_enter_alpha.current_script.src.split('/').pop();

for(var k = 0; k < umsplugin_enter_alpha.current_script.attributes.length; k++)
{
	var attr = umsplugin_enter_alpha.current_script.attributes[k];
	if (attr.name.substring(0,4) === 'data')
	{
		var data_name = attr.name.substr(5);
		var data_value = attr.value;
		umsplugin_enter_alpha.options[data_name] = data_value;
	}
}

var umsplugins = umsplugins || {};
umsplugins.Tenter_alpha = function($, objname, options)
{
	'use strict';
	var self = this;
	var $w = $(window);

	this.init = function(options)
	{
		self.options = options;
		self.initVars();
		self.processTriggers();
		self.onElementsEvents();
	};

	this.initVars = function()
	{
		self.trigger_show = options.trigger_show;
		self.active = true;
	};

	this.onReady = function()
	{
		// CODE ON APLICATION IS READY
		if ($.fn.umsplugins_dom_visible !== true)
		{
			throw new Error('Library jqueryUmsDomVisible is required!!!');
		}	
		self.start();
	};

	this.start = function()
	{
		// CODE ON APLICATION IS STARTED
		self.triggerStarted();
	};

	this.processTriggers = function()
	{

	};

	this.onElementsEvents = function()
	{
		$w.scroll
		(
			function()
			{
				self.processScroll();
			}
		);
	};

	this.processScroll = function()
	{
		if (self.active)
		{
			self.processEnterAlpha();
		}
	};

	this.processEnterAlpha = function()
	{
		$('.enter-alpha').each
		(
			function(index, el)
			{
				self.processElement($(el));
			}
		);
	};

	this.triggerElement = function($element)
	{
		$element.removeClass('enter-alpha').addClass('enter-alpha-active');
		self.active = ( $('.enter-alpha').length > 0 );
	};

	this.processElement = function($element)
	{
		var trs = $element.attr('data-trigger_show') || self.trigger_show;
		if (trs === 'visible')
		{
			var vis = $element.visible();
			if (vis.visible && vis.type === 'inside')
			{
				self.triggerElement($element);
			}
		}
		else
		{
			var header_height = ($('header').height() + 0);
			var data = $element[0].getBoundingClientRect();
			var top  = data.top - header_height;
			var wh   = ($w.height() - header_height);
			var pc   = (top * 100) / wh;
			var vrf  = 100 - pc;
			if (vrf >= trs)
			{
				self.triggerElement($element);
			}
		}
	};

	CjsBaseClass.call(this, $, objname, options);

	this.init(options);
};

umsplugins.enter_alpha = new umsplugins.Tenter_alpha
(
	window.cjsbaseclass_jquery,
	'enter_alpha',
	{
		'debug'        : CJS_DEBUG_MODE_0,
		'highlighted'  : 'auto',
		'trigger_show' : parseInt(umsplugin_enter_alpha.options.trigger_show)
	}
);