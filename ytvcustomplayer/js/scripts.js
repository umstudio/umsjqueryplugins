$(document).ready(function() {
	
	$('.video').YoutubeCustomPlayer
	(
		{
			youtube:
			{
				autoplay: true
			},
			visual:
			{
				buttons:
				{
					close:
					{
						show: true,
						onClick: function(p_video_box, p_player)
						{
							p_player.pauseVideo();
							$(p_video_box).fadeOut();
							$('#btnShowVideo').show(0);
						}
					},
					play:
					{
						show: true
					}
				}
			},
			events:
			{
				onReady: function(player)
				{
					console.log('CONSOLE => READY', player);
				},
				onPlay: function(player)
				{
					console.log('CONSOLE => PLAY', player);
				},
				onPause: function(player)
				{
					console.log('CONSOLE => PAUSE', player);
				},
				onFinished: function(player)
				{
					console.log('CONSOLE => FINISHED', player);
				},
				onClose: function(p_div_box)
				{
					console.log('CONSOLE => CLOSE', p_div_box);
				}
			}
		}
	);

	$(document).on
	(
		'click touchstart',
		'#btnShowVideo',
		function(e)
		{
			e.preventDefault();
			$(this).hide();
			$('.video-box:eq(0)').show(0);
			//$('.video:eq(0)').YoutubeCustomPlayer('play');
		}
	);

});