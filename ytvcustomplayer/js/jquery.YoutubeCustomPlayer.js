/*jshint multistr: true */

var ytcp = 
{
	youTubeIframeAPIReady: false,
	loadYoutubeScript: function()
	{
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	}
};

function onYouTubeIframeAPIReady()
{
	ytcp.youTubeIframeAPIReady = true;
	$(document).trigger('YoutubeCustomPlayerReady');
}

$(document).ready(function() {
	ytcp.loadYoutubeScript();
});

(function($){

	var defaults =
	{
		videoId: '',
		youtube:
		{
			autoplay: true
		},
		visual:
		{
			buttons:
			{
				close:
				{
					show: true,
					onClick: null
				},
				play:
				{
					show: true,
				}
			}
		},
		events: 
		{
			onReady    : null,
			onPlay     : null,
			onPause    : null,
			onFinished : null,
			onClose    : null,
		}
	};
	var objects = [];

	var methods =
	{
		init : function(options)
		{
			var opts = $.extend( {}, defaults, options );

			var result = this.each(function()
			{
				$.fn.YoutubeCustomPlayer.process($(this), opts, $(this).attr('data-video'));
			});

			$(document).on
			(
				'YoutubeCustomPlayerReady',
				function()
				{
					$.fn.YoutubeCustomPlayer._mountPlayers();
				}
			);

			$(document).ready(function() {

				$.fn.YoutubeCustomPlayer.monitor();

				$(document).on
				(
					'click touchstart',
					'.video-box-control-play',
					function(e)
					{
						e.preventDefault();
						var ids = $(this).closest('.video-box').attr('data-ytcp-ids');
						if ($(this).hasClass('play'))
						{
							objects[ids].player.playVideo();
						}
						else
						{
							objects[ids].player.pauseVideo();
						}
					}
				);

				$(document).on
				(
					'click touchstart',
					'.video-box-close',
					function(e)
					{
						e.preventDefault();
						var ids = $(this).closest('.video-box').attr('data-ytcp-ids');
						if (objects[ids].options.visual.buttons.close.onClick !== null)
						{
							objects[ids].options.visual.buttons.close.onClick((this).closest('.video-box'), objects[ids].player);
							if (objects[ids].options.events.onClose !== null)
							{
								objects[ids].options.events.onClose(objects[ids].box);
							}
						}
					}
				);

				$(document).on
				(
					'click touchstart',
					'.video-box-control-progress,.video-box-control-buffer,.video-box-control-progress-box',
					function(e)
					{
						e.preventDefault();
						var ids = $(this).closest('.video-box').attr('data-ytcp-ids');

						var largura  = parseInt($(this).closest('.video-box-control-progress-box').css('width'));
						
						var position = e.offsetX;
						if (e.originalEvent.touches !== undefined)
						{
							position = e.originalEvent.touches[0].pageX - parseInt($(e.originalEvent.target).closest('.video-box-control-progress-box').css('margin-left'));
						}

						var prcpos   = (position * 100) / largura;

						var player = objects[ids].player;
						var total  = player.getDuration();
						var moveto = (prcpos * total) / 100;

						player.seekTo(moveto);
					}
				);

			});

			return result;
		},
		play : function() { return this.each(function() { $.fn.YoutubeCustomPlayer.play(this); }); },

	};

	$.fn.YoutubeCustomPlayer = function(methodOrOptions)
	{
		if ( methods[methodOrOptions] )
		{
			return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof methodOrOptions === 'object' || ! methodOrOptions )
		{
			return methods.init.apply( this, arguments );
		}
		else
		{
			$.error('Method ' + methodOrOptions + ' does not exist on jQuery.YoutubeCustomPlayer');
		}
	};

	$.fn.YoutubeCustomPlayer.play = function(p_args)
	{
		var ids = $(p_args).attr('data-ytcp-ids');
		//objects[ids].box.show(0);
		objects[ids].player.playVideo();
	};

	$.fn.YoutubeCustomPlayer.makeId = function()
	{
		var text = "";
		var possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789";
		var pl = possible.length;
		for(var i = 0; i < 8; i++)
		{
			text += possible.charAt(Math.floor(Math.random() * pl));
		}
		return text;
	};

	$.fn.YoutubeCustomPlayer.process = function(p_elem, p_options, p_video_id)
	{
		var ids = $.fn.YoutubeCustomPlayer.makeId();
		while (objects[ids] !== undefined)
		{
			ids = $.fn.YoutubeCustomPlayer.makeId();
		}
		p_elem.attr('data-ytcp-ids', ids);
		if (p_elem.attr('id') === undefined)
		{
			p_elem.attr('id', ids);
		}

		p_elem.wrap('<div class="video-box" data-ytcp-ids="' + ids + '" id="video-box-' + ids + '"></div>');
		if (p_options.visual.buttons.close.show)
		{
			$('#video-box-' + ids).prepend('<div class="video-box-close"></div>');
		}
		var str_play = '';
		if (p_options.visual.buttons.play.show)
		{
			str_play = '<div class="video-box-control-play play"></div>';
		}

		$('#video-box-' + ids).append
		(
			'<div class="video-box-controls"> \
				' + str_play + ' \
				<div class="video-box-control-progress-box"> \
					<div class="video-box-control-time">00:00 | 00:00</div> \
					<div class="video-box-control-buffer"></div> \
					<div class="video-box-control-progress"></div> \
				</div> \
			</div>'
		);

		var comp = {};
		comp.player  = null;
		comp.ids     = ids;
		comp.box     = $('#video-box-' + ids);
		comp.videoId = p_video_id;
		comp.element = p_elem;
		comp.options = p_options;

		objects[ids] = (comp);
	};

	$.fn.YoutubeCustomPlayer.monitor = function()
	{
		for(var k in objects)
		{
			var obj = objects[k];
			var player = obj.player;
			if (player !== null)
			{
				if (player.getVideoLoadedFraction !== undefined)
				{
					var int_time_current = player.getCurrentTime();
					var int_time_total   = player.getDuration();
					
					var fmt_time_current = $.fn.YoutubeCustomPlayer._formatSeconds(int_time_current);
					var fmt_time_total   = $.fn.YoutubeCustomPlayer._formatSeconds(int_time_total);

					var buffer = (player.getVideoLoadedFraction() * 100);
					var position = ((int_time_current * 100) / int_time_total);

					$('#video-box-' + obj.ids + ' .video-box-control-buffer').css('width', buffer + '%');
					$('#video-box-' + obj.ids + ' .video-box-control-progress').css('width', position + '%');
					$('#video-box-' + obj.ids + ' .video-box-control-time').html(fmt_time_current + '&nbsp;|&nbsp;' + fmt_time_total);
				}
			}
		}

		setTimeout(function() {
			$.fn.YoutubeCustomPlayer.monitor();
		}, 250);
	};

	$.fn.YoutubeCustomPlayer._formatSeconds = function(p_seconds)
	{
		var date = new Date(p_seconds * 1000);
		var hh = date.getUTCHours();
		var mm = date.getUTCMinutes();
		var ss = date.getSeconds();
		if (hh < 10) {hh = "0"+hh;}
		if (mm < 10) {mm = "0"+mm;}
		if (ss < 10) {ss = "0"+ss;}
		if (hh > 0)
		{
			return hh + ':' + mm + ':' + ss;
		}
		else
		{
			return mm + ':' + ss;
		}
	};

	$.fn.YoutubeCustomPlayer._mountPlayers = function()
	{
		for(var k in objects)
		{
			var obj = objects[k];
			if (obj.player === null)
			{
				$.fn.YoutubeCustomPlayer._mountPlayer(obj);
			}
		}
	};

	$.fn.YoutubeCustomPlayer._mountPlayer = function(p_obj)
	{
		p_obj.player = new YT.Player
		(
			p_obj.element.attr('id'),
			{
				height     : '390',
				width      : '640',
				videoId    : p_obj.videoId,
				playerVars :
				{
					'autoplay'      : (p_obj.options.youtube.autoplay === true) ? 1 : 0,
					'controls'      : 0,
					'showinfo'      : 0,
					'rel'           : 0,
					'modestbranding': 1
				},
				events:
				{
					'onReady': function()
					{
						if (p_obj.options.events.onReady !== null) { p_obj.options.events.onReady(p_obj.player); }
					},
					'onStateChange': function(p_state)
					{
						switch (p_state.data)
						{
							case -1:
								// NÃO INICIADO
							break;
							case 0:
								if (p_obj.options.events.onFinished !== null) { p_obj.options.events.onFinished(p_obj.player); }
							break;
							case 1:
								$('#video-box-' + p_obj.ids + ' .video-box-control-play').toggleClass('pause play');
								if (p_obj.options.events.onPlay !== null) { p_obj.options.events.onPlay(p_obj.player); }
							break;
							case 2:
								$('#video-box-' + p_obj.ids + ' .video-box-control-play').toggleClass('pause play');
								if (p_obj.options.events.onPause !== null) { p_obj.options.events.onPause(p_obj.player); }
							break;
							case 3:
								// STATE VIDEO BUFFERING
							break;
							case 5:
								// STATE VIDEO CUED
							break;
						}
					}
				}
			}
		);
	};

})( jQuery );