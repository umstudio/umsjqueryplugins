# UMS Dom Visible
> Plugin para detectar se um elemento DOM está visível, que tipo e quanto dele está.

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Demo
<http://umstudiohomolog.com.br/ums/jquery-plugins/jqueryUmsDomVisible/demo/>

### Estrutura Básica (HTML)
```html
<script src="js/cjsbaseclass.min.js"></script>
<script src="js/ums_dom_visible.min.js"></script>
```
### Dependências
* Não há