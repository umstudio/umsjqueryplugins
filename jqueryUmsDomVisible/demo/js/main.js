/* global CjsBaseClass,CJS_DEBUG_MODE_0,CJS_DEBUG_MODE_1,CJS_DEBUG_MODE_2 */
var umsapp = umsapp || {};
umsapp.Tmain = function($, objname, options)
{
	'use strict';
	var self = this;
	var $w = $(window);

	this.init = function()
	{
		self.initVars();
		self.processTriggers();
		self.onElementsEvents();
	};

	this.initVars = function()
	{
		self.$elements = $('section');
		self.$menus = $('header > ul li');
	};

	this.onReady = function()
	{
		// CODE ON APLICATION IS READY
		self.start();
	};

	this.start = function()
	{
		// CODE ON APLICATION IS STARTED
		self.triggerStarted();
	};

	this.processTriggers = function()
	{

	};

	this.onElementsEvents = function()
	{
		$w.scroll
		(
			function()
			{
				self.processScroll();
			}
		);
	};

	this.processScroll = function()
	{
		var actives = [];
		var $element;
		self.$elements.each
		(
			function(index, el)
			{
				$element = $(el);
				var is_visible = $element.is_visible();
				if (is_visible)
				{
					actives.push( $element.attr('id') );
				}
			}
		);

		var $dom_li, $dom_a;
		for(var k = 0; k < self.$menus.length; k++)
		{
			$dom_li = $(self.$menus[k]);
			$dom_a  = $dom_li.find('a');
			var ids = $dom_a.attr('data-section');

			if (actives.indexOf(ids) >= 0)
			{
				$dom_li.addClass('active');
				
				$element = $('section.' + ids);
				var _visible = $element.visible();
				$element.find('p').html('type: ' + _visible.type + ' | area: ' + _visible.area);
			}
			else
			{
				$dom_li.removeClass('active');
			}
		}
	};

	CjsBaseClass.call(this, $, objname, options);

	this.init();
};

umsapp.main = new umsapp.Tmain
(
	window.cjsbaseclass_jquery,
	'main',
	{
		'debug'         : CJS_DEBUG_MODE_1,
		'highlighted'   : 'auto'
	}
);