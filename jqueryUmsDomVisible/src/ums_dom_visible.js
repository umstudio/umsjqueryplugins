var umsplugins = umsplugins || {};

$.fn.umsplugins_dom_visible = true;

$.fn.visible = function(p_exact)
{
	var $t      = $(this);
	var $w      = $(window);
	var wh      = $w.height();
	var at      = wh - hh;
	var hh      = ($('header').length > 0) ? ($('header').height() + 0) : 0;
	var cr      = $t[0].getBoundingClientRect();
	var visible = false;

	if (typeof Number.prototype.trunc2 !== 'function')
	{
		Number.prototype.trunc2 = function() { return parseFloat(this.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]); };
	}

	visible = ( (cr.top === hh) && (cr.bottom === wh) );
	if (visible)
	{
		return {
			'visible' : true,
			'type'    : 'exact',
			'area'    : 100
		};
	}

	visible = ( (cr.top < hh) && (cr.bottom > wh) );
	if (visible)
	{
		return {
			'visible' : true,
			'type'    : 'full',
			'area'    : 100
		};
	}

	visible = ( (cr.top > hh) && (cr.bottom < wh) );
	if (visible)
	{
		return {
			'visible' : true,
			'type'    : 'inside',
			'area'    : (($t.height() * 100) / wh).trunc2()
		};
	}

	visible = ( (cr.top >= hh) && (cr.top < wh) && (cr.bottom > wh) );
	if (visible)
	{
		return {
			'visible' : true,
			'type'    : 'top',
			'area'    : (( (wh - cr.top)  * 100) / wh).trunc2()
		};
	}

	visible = ( (cr.top < hh) && (cr.bottom <= wh) && (cr.bottom > hh) );
	if (visible)
	{
		return {
			'visible' : true,
			'type'    : 'bottom',
			'area'    : (( (cr.bottom - hh)  * 100) / wh).trunc2()
		};
	}

	return {
		'visible' : false,
		'type'    : 'none',
		'area'    : 0
	};
};

$.fn.is_visible = function(p_exact)
{
	var wh = $(window).height();
	var hh = $('header').height() + 0;
	var cr = $(this)[0].getBoundingClientRect();

	var visible = 
		( (cr.top === hh) && (cr.bottom === wh) )                 ||
		( (cr.top < hh) && (cr.bottom > wh) )                     ||
		( (cr.top > hh) && (cr.bottom < wh) )                     ||
		( (cr.top >= hh) && (cr.top < wh) && (cr.bottom > wh) )    ||
		( (cr.top < hh) && (cr.bottom <= wh) && (cr.bottom > hh) ) 
	;
	return visible;
};