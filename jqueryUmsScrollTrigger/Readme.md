# UMS Scroll Trigger

Plugin para disparar eventos durante o scroll da página em relação aos elementos.

### Compatível
	* Google Chrome
	* Firefox
	* Safari
	* Internet Explorer (9+)

### Demo
http://umstudiohomolog.com.br/ums/jquery-plugins/jqueryUmsScrollTrigger/demo/

### Estrutura Básica (HTML)
```html
<script type="text/javascript" src="js/jqueryUmsScrollTrigger.min.js"></script>
```
### Chamada Plugin

```js
	$('#section-8,#section-14,#section-21').umsScrollTrigger
	(
		{
			'debug'    : true,
			'triggers' : true,
			'showline' : true,
			'target'   : 30, // 10%, 20%, 30%, 40%, 'middle', 60%, 70%, 80%, 90%
			'once'     : 
			{
				'up'   : false,
				'down' : false,
				'both' : false
			},
			'events' :
			{
				'onCollision' : function(args)
				{
					console.log(args);
				}
			}
		}
	);
```

### Dependências
* [jQuery 2.x]

[jQuery 2.x]:http://code.jquery.com/