(function($){

	var defaults =
	{
		'debug'    : false,
		'triggers' : false,
		'showline' : false,
		'target'   : 'middle', // 10%, 20%, 30%, 40%, 'middle', 60%, 70%, 80%, 90%
		'once'     : 
		{
			'up'   : false,
			'down' : false,
			'both' : false
		},
		'events':
		{
			'onCollision' : null
		}
	};
	var options = {};
	var objects = [];
	var catchResize = false;
	var metrics = 
	{
		'window':
		{
			'height'    : window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
		},
		'scroll':
		{
			'last'      : 0,
			'current'   : pageYOffset || (document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop),
			'direction' : 'unknown'
		}
	};
	var getLinePos = function(p_window_height, p_postrg, p_scrolltop)
	{
		return ((p_window_height * p_postrg) / 100) + p_scrolltop;
	};

	var methods =
	{
		init : function(p_options)
		{
			options = $.extend(true, {}, defaults, p_options);
			var result = this.each(function()
			{
				if (typeof $(this).attr('id') !== 'string')
				{
					$.error('The element needs attribute ID');
				}
				$.fn.umsScrollTrigger.process($(this), options);
			});
			return result;
		}
	};

	$.fn.umsScrollTrigger = function(methodOrOptions)
	{
		if ( methods[methodOrOptions] )
		{
			return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof methodOrOptions === 'object' || ! methodOrOptions )
		{
			return methods.init.apply( this, arguments );
		}
		else
		{
			$.error('Method ' + methodOrOptions + ' does not exist on jQuery.umsScrollTrigger');
		}
	};

	$.fn.umsScrollTrigger.trigger = function(p_trigger_name, p_debug, p_args)
	{
		if (p_debug === true)
		{
			console.log(p_trigger_name, p_args);
		}
		$(document).trigger(p_trigger_name, p_args);
	};

	$.fn.umsScrollTrigger.refreshMetrics = function()
	{
		metrics.scroll.last      = metrics.window.scrolltop;
		metrics.window.height    = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		metrics.scroll.current   = pageYOffset || (document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop);
		metrics.scroll.direction = (metrics.scroll.last > metrics.window.scrolltop) ? 'up' : 'down';
	};
	$.fn.umsScrollTrigger.refreshMetrics();

	$.fn.umsScrollTrigger.makeId = function()
	{
		var text = "";
		var possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789";
		var pl = possible.length;
		for(var i = 0; i < 8; i++)
		{
			text += possible.charAt(Math.floor(Math.random() * pl));
		}
		return text;
	};

	$.fn.umsScrollTrigger.addUniqueId = function(p_elem)
	{
		var ids = $.fn.umsScrollTrigger.makeId();
		while (objects[ids] !== undefined)
		{
			ids = $.fn.umsScrollTrigger.makeId();
		}
		p_elem.attr('data-ums-scrtrigger-ids', ids);
		return ids;
	};

	$.fn.umsScrollTrigger.process = function(p_elem, p_options)
	{
		var ids = $.fn.umsScrollTrigger.addUniqueId(p_elem);

		var element_class = p_elem.attr('class');
		element_class = (element_class === undefined) ? 'ums-scrtrigger' : element_class + '-ums-scrtrigger';

		var element_addclass = p_elem.attr('id');
		element_addclass = (element_addclass === undefined) ? '' : ' ' + element_addclass + '-ums-scrtrigger';

		if (element_class.trim() == element_addclass.trim()) { element_addclass = ''; }

		var postrg = (p_options.target == 'middle') ? '50%' : p_options.target;
		postrg = postrg.toString().replace('%', '');

		var offset_top = p_elem.offset().top;

		var comp = {};
		comp.id      = ids;
		comp.process = 
		{
			'up': true,
			'down': true
		};
		comp.canproc = false;
		comp.element = p_elem;
		comp.pos     = offset_top;
		comp.bottom  = comp.pos + p_elem.outerHeight();
		comp.line    = 
		{
			last: -1,
			curr: offset_top,
		};
		comp.eclass  = element_class;
		comp.options = p_options;
		comp.postrg  = postrg;
		objects[ids] = (comp);

		if (p_options.showline)
		{
			if ($('.debug-line[data-target="' + postrg + '"]').length === 0)
			{
				var innerdiv  = '<div style="position: absolute; right: 10px; background-color: #FBFFD0; border: 1px solid red; bottom: -1px; padding: 3px; font-size: x-small; -webkit-border-top-left-radius: 6px; -webkit-border-top-right-radius: 6px; -moz-border-radius-topleft: 6px; -moz-border-radius-topright: 6px; border-top-left-radius: 6px; border-top-right-radius: 6px;">#' + p_elem.attr('id') + '</div>';
				var outterdiv = '<div id="debug-line-' + ids + '" class="debug-line" data-target="' + postrg + '" style="position: absolute; top: 0px; left: 0px; height: 1px; border-bottom: 1px solid red; width: 100%;">' + innerdiv + '</div>';
				$('body').append(outterdiv);
			}
			else
			{
				$('.debug-line[data-target="' + postrg + '"] div:eq(0)').append(', #' + p_elem.attr('id'));
			}
		}

		if (!catchResize)
		{
			catchResize = true;
			window.onresize = function()
			{
				$.fn.umsScrollTrigger.processScroll();
			};
			window.onscroll = function()
			{
				$.fn.umsScrollTrigger.processScroll();
			};
		}

		$.fn.umsScrollTrigger.refreshMetrics();
		$.fn.umsScrollTrigger.recalcule(comp);

		objects[ids].canproc = true;
	};

	$.fn.umsScrollTrigger.processScroll = function()
	{
		$.fn.umsScrollTrigger.refreshMetrics();
		if (metrics.scroll.last != metrics.scroll.current)
		{
			for(var k in objects)
			{
				$.fn.umsScrollTrigger.recalcule(objects[k]);
			}
		}
	};

	$.fn.umsScrollTrigger.recalcule = function(object)
	{
		var line;
		if (object.canproc)
		{
			object.line.curr = getLinePos(parseInt(metrics.window.height), parseInt(object.postrg), parseInt(metrics.scroll.current.toString()));
			object.pos       = object.element.offset().top;
			object.bottom    = object.pos + object.element.outerHeight();

			if (object.process.down || object.process.up)
			{
				var gotcha = false;
				var test_collision = 
				{
					'up':
					{
						'top'   : (object.line.last < object.pos   ) && (object.line.curr >= object.pos  ),
						'bottom': (object.line.last < object.bottom) && (object.line.curr >= object.bottom)
					},
					'down':
					{
						'top'   : (object.line.last >= object.pos   ) && (object.line.curr < object.pos),
						'bottom': (object.line.last >= object.bottom) && (object.line.curr < object.bottom)
					}
				};

				var args;
				if ( test_collision.up.top || test_collision.up.bottom )
				{
					if (object.process.up)
					{
						gotcha = true;
						if ( (object.options.once.up) || (object.options.once.both) )
						{
							object.process.up = false;
						}
						if ( (object.options.events.onCollision !== null) || (object.options.trigger) )
						{
							args =
							{
								'object'   : object,
								'id'       : object.element.attr('id'),
								'direction': 'up',
								'targetpos': test_collision.up
							};

							if (object.options.events.onCollision !== null)
							{
								object.options.events.onCollision(args);
							}

							if (object.options.triggers)
							{
								$.fn.umsScrollTrigger.trigger('umsScrollTriggerOnCollision', object.options.debug, args);
							}
						}
					}
				}
				else if ( test_collision.down.top || test_collision.down.bottom )
				{
					if (object.process.down)
					{
						gotcha = true;
						if ( (object.options.once.down) || (object.options.once.both) )
						{
							object.process.down = false;
						}
						if ( (object.options.events.onCollision !== null) || (object.options.trigger) )
						{
							args = 
							{
								'object'   : object,
								'id'       : object.element.attr('id'),
								'direction': 'down',
								'targetpos': test_collision.down
							};

							if (object.options.events.onCollision !== null)
							{
								object.options.events.onCollision(args);
							}

							if (object.options.triggers)
							{
								$.fn.umsScrollTrigger.trigger('umsScrollTriggerOnCollision', object.options.debug, args);
							}
						}
					}
				}
			}

			object.line.last = object.line.curr;

			if (object.options.showline)
			{
				$('#debug-line-' + object.id).css('top', object.line.curr);
			}
		}
	};

})( jQuery );