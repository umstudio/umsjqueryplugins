(function ($) {
    $.fn.shake = function (options) {
        // defaults
        var settings = {
            'shakes': 2,
            'distance': 10,
            'duration': 400
        };
        // merge options
        if (options) {
            $.extend(settings, options);
        }
        // make it so
        var pos;
        return this.each(function () {
            $this = $(this);
            // position if necessary
            pos = $this.css('position');
            if (!pos || pos === 'static') {
                $this.css('position', 'relative');
            }
            // shake it
            for (var x = 1; x <= settings.shakes; x++) {
                $this.animate({ left: settings.distance * -1 }, (settings.duration / settings.shakes) / 4)
                    .animate({ left: settings.distance }, (settings.duration / settings.shakes) / 2)
                    .animate({ left: 0 }, (settings.duration / settings.shakes) / 4);
            }
        });
    };
}(jQuery));

var nice;

$(document).ready(function() {

	$('#section-8,#section-14,#section-21').umsScrollTrigger
	(
		{
			'debug'    : true,
			'triggers' : true,
			'showline' : true,
			'target'   : 30, // 10%, 20%, 30%, 40%, 'middle', 60%, 70%, 80%, 90%
			'once'     : 
			{
				'up'   : false,
				'down' : false,
				'both' : false
			},
			'events' :
			{
				'onCollision' : function(args)
				{
					console.log(args);
					args.object.element.shake();
				}
			}
		}
	);

	$('#section-21').umsScrollTrigger
	(
		{
			'debug'    : true,
			'triggers' : true,
			'showline' : true,
			'target'   : 70, // 10%, 20%, 30%, 40%, 'middle', 60%, 70%, 80%, 90%
			'once'     : 
			{
				'up'   : false,
				'down' : false,
				'both' : false
			},
			'events':
			{
				'onCollision' : function(args)
				{
					console.log(args);
					args.object.element.shake();
				}
			}
		}
	);

});