<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>jqueryUmsScrollTrigger - Demo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<script type="text/javascript" src="js/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="../src/jqueryUmsScrollTrigger.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</head>
<body>
	<?php for($k = 1; $k <= 30; $k++) { ?>
		<section id="section-<?php echo $k; ?>">Section <?php echo $k; ?> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus fuga sequi accusamus, quidem, officiis vitae eum minima aliquam mollitia! Impedit hic, optio quo consequuntur cupiditate corporis nulla, provident enim odio!</section>
	<?php } ?>
</body>
</html>