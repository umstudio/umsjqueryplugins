var TYoutubePlayer = (function(){
	'use strict';
	var self;

	return {
		init : function()
		{
			self = this;
			self.initVars();
			self.processTriggers();
			self.initYoutube();
			self.trigger('TYoutubePlayer-ready');
		},

		initVars: function()
		{
			self.debug = true;
			self.player = undefined;
			self.ytr = false;
		},

		onYouTubeIframeAPIReady: function()
		{
			self.ytr = true;
			self.trigger('youtube-api-ready');
		},

		initYoutube: function()
		{
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/iframe_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		},

		trigger: function(p_trigger_name, p_args)
		{
			if (p_args === undefined)
			{
				$(document).trigger(p_trigger_name);
				if (self.debug === true) { console.log('Trigger: ' + p_trigger_name); }
			}
			else
			{
				$(document).trigger(p_trigger_name, p_args);
				if (self.debug === true) { console.log('Trigger: ' + p_trigger_name, p_args); }
			}
		},

		processTriggers: function()
		{
			$(document).on
			(
				'otherclass-ready',
				function()
				{
					
				}
			);
		},

		play: function(p_options)
		{
			$('#player').replaceWith('<div id="player"></div>');
			self.player = new YT.Player
			(
				'player',
				{
					height  : p_options.size[0],
					width   : p_options.size[0],
					playerVars: { 'autoplay': p_options.autoplay, 'controls': p_options.controls },
					videoId : p_options.id,
					events  :
					{
						'onReady'      : function() { self.trigger('youtube-player-onready', p_options); },
						'onStateChange': function(event)
						{
							try
							{
								switch (event.data)
								{
									case YT.PlayerState.PLAYING:
										self.trigger('youtube-player-on-play', p_options);
									break;
									case YT.PlayerState.PAUSED:
										self.trigger('youtube-player-on-paused', p_options);
									break;
									case YT.PlayerState.BUFFERING:
										self.trigger('youtube-player-on-buffering', p_options);
									break;
									case YT.PlayerState.CUED:
										self.trigger('youtube-player-on-cued', p_options);
									break;
									case YT.PlayerState.ENDED:
										self.trigger('youtube-player-on-ended', p_options);
									break;
								}
							}
							catch(ex)
							{

							}
						},
					}
				}
			);
		},

		stop: function()
		{
			if (self.player !== undefined)
			{
				self.player.stopVideo();
			}
		},

		destroy: function()
		{
			if (self.player !== undefined)
			{
				$('#player').replaceWith('<div id="player"></div>');
				self.player = undefined;
			}
		}
	};

})();

$(document).ready(function() {
	TYoutubePlayer.init();
});

function onYouTubeIframeAPIReady()
{
	TYoutubePlayer.onYouTubeIframeAPIReady();
}

setTimeout
(
	function()
	{
		var options = 
		{
			'id'       : 'M7lc1UVf-VE',
			'size'     : [390, 640],
			'autoplay' : 1,
			'controls' : 1
		};
		TYoutubePlayer.play(options);
	},
	1000
);