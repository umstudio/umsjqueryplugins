var umsplugins = umsplugins || {};
umsplugins.Tsections_observer = function($, objname, options)
{
	'use strict';
	var self = this;
	var $w = $(window);

	this.create = function()
	{
		self.last_section = '';
		self.last_pos = -1;
		self.events.onCreate();
	};

	this.onReady = function()
	{
		// CODE ON APLICATION IS READY
		if ($.fn.umsplugins_dom_visible !== true)
		{
			throw new Error('Library jqueryUmsDomVisible is required!!!');
		}
		self.events.onReady();
	};

	this.start = function()
	{
		// CODE ON APLICATION IS STARTED
		setTimeout(function() { self.processScroll(); }, 1 );
		self.events.onStarted();
	};

	this.processTriggers = function()
	{

	};

	this.onElementsEvents = function()
	{
		$w.scroll
		(
			function()
			{
				self.processScroll();
			}
		);

		$w.resize
		(
			function()
			{
				self.processScroll();
			}
		);
	};

	this.processScroll = function()
	{
		var max_area = 0;
		var $data;
		var $element;
		var $t;

		$('section').filter
		(
			function(index)
			{
				$t = $(this);
				if ($t.is_visible())
				{
					$data = $t.visible();
					if ($data.area > max_area)
					{
						max_area = $data.area;
						$element = $t;
					}
				}
			}
		);

		if ($element !== undefined)
		{
			var ids = $element.attr('id');
			if (ids !== self.last_section)
			{
				var top = $(window).scrollTop();
				var dir = (top > self.last_pos) ? 'D' : 'U';
				self.last_pos = top;

				var last = self.last_section;
				self.last_section = ids;
				self.trigger('on-section-change', { 'element': $element, 'last': last, 'id': ids, 'dir': dir } );
			}
		}
	};

	CjsBaseClass.call(this, $, objname, options);
};

umsplugins.sections_observer = new umsplugins.Tsections_observer
(
	window.cjsbaseclass_jquery,
	'sections_observer',
	{
		'debug'       : CJS_DEBUG_MODE_0,
		'highlighted' : 'auto'
	}
);