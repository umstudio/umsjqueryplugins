# UMS Sections Observer
> Plugin para disparar eventos durante a troca de seções do site on scroll e on resize.

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Demo
<http://umstudiohomolog.com.br/ums/jquery-plugins/jqueryUmsSectionObserver/demo/>

### Estrutura Básica (HTML)
```html
<script src="js/cjsbaseclass.min.js"></script>
<script src="js/ums_sections_observer.min.js"></script>
```
### Chamada Plugin
```js
    self.onTrigger
    (
        'on-section-change',
        function(p_args)
        {
            self.log.info(p_args.id);
            $('header ul li.active').removeClass('active');
            p_args.element.active_menu();
        }
    );
```

### Dependências
* [cjsbaseclass]

[cjsbaseclass]:https://www.npmjs.com/package/cjsbaseclass