(function($) {

	if (typeof $.fn.activate === 'undefined')
	{
		$.fn.activate = function(p_exact)
		{
			return this.each
			(
				function()
				{
					$(this).addClass('active');
				}
			);
		};
	}

	if (typeof $.fn.inactivate === 'undefined')
	{
		$.fn.inactivate = function(p_exact)
		{
			return this.each
			(
				function()
				{
					$(this).removeClass('active');
				}
			);
		};
	}

	if (typeof $.fn.active_menu === 'undefined')
	{
		$.fn.active_menu = function()
		{
			return this.each
			(
				function()
				{
					$('header ul li a[href = "#' + $(this).attr('id') + '"]').parent().addClass('active');
				}
			);
		};
	}

	if (typeof $.fn.inactive_menu === 'undefined')
	{
		$.fn.inactive_menu = function()
		{
			return this.each
			(
				function()
				{
					$('header ul li a[href = "#' + $(this).attr('id') + '"]').parent().removeClass('active');
				}
			);
		};
	}

	if (typeof $.fn.clear_menu === 'undefined')
	{
		$.fn.clear_menu = function()
		{
			return this.each
			(
				function()
				{
					$('header ul li.active').removeClass('active');
				}
			);
		};
	}
	
}(jQuery));

var umsapp = umsapp || {};
umsapp.Tmain = function($, objname, options)
{
	'use strict';
	var self = this;

	this.create = function()
	{
		self.events.onCreate();
	};

	this.onReady = function()
    {
        self.events.onReady();
    };
 
    this.start = function()
    {
        self.events.onStarted();
    };

	this.processTriggers = function()
	{
		self.onTrigger
		(
			'on-section-change',
			function(p_args)
			{
				self.log.info(p_args);
				$('header ul li.active').clear_menu();
				p_args.element.active_menu();
			}
		);
	};

	this.onElementsEvents = function()
	{

	};

	CjsBaseClass.call(this, $, objname, options);
};

umsapp.main = new umsapp.Tmain
(
	window.cjsbaseclass_jquery,
	'main',
	{
		'debug'       : CJS_DEBUG_MODE_1,
		'highlighted' : 'auto'
	}
);