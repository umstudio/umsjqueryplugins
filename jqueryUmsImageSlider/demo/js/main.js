$(document).ready(function() {

	$('#gallery01').umsImageSlider
	(
		{
			'images':
			[
				'http://placehold.it/350x150&text=1',
				'http://placehold.it/350x150&text=2',
				'http://placehold.it/350x150&text=3',
				'http://placehold.it/350x150&text=4',
				'http://placehold.it/350x150&text=5',
			],
			'nav':
			{
				'captions':
				{
					'prev': 'Anterior|',
					'next': 'Próximo'
				}
			},
			'cols'  : 3,
			'easing': 'easeOutExpo',
			'speed' : 500
		}
	);

});