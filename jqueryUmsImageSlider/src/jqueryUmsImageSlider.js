(function($){

	var defaults =
	{
		'images'     : [],
		'cols'       : 2,
		'easing'     : 'easeInOutBack',
		'speed'      : 500,
		'nav'        : 
		{
			'show': true,
			'captions':
			{
				'next': 'Next',
				'prev': 'Prev',
			}
		},
	};
	var options = {};
	var objects = [];

	var methods =
	{
		init : function(p_options)
		{
			var opts = $.extend(true, {}, defaults, p_options);
			return this.each(function()
			{
				$.fn.umsImageSlider.process($(this), opts);
			});
		},
		next: function()      { return this.each(function() { $.fn.umsImageSlider.next(this); }); },
		prev: function()      { return this.each(function() { $.fn.umsImageSlider.prev(this); }); },
		goto: function(p_page){ return this.each(function() { $.fn.umsImageSlider.goto(this, p_page); }); }
	};

	$.fn.umsImageSlider = function(methodOrOptions)
	{
		if ( methods[methodOrOptions] )
		{
			return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof methodOrOptions === 'object' || ! methodOrOptions )
		{
			return methods.init.apply( this, arguments );
		}
		else
		{
			$.error('Method ' + methodOrOptions + ' does not exist on jQuery.umsImageSlider');
		}
	};

	$.fn.umsImageSlider.makeId = function()
	{
		var text = "";
		var possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789";
		var pl = possible.length;
		for(var i = 0; i < 8; i++)
		{
			text += possible.charAt(Math.floor(Math.random() * pl));
		}
		return text;
	};

	$.fn.umsImageSlider.lock = function(p_elem)
	{
		$(p_elem).attr('data-lock', '1');
	};

	$.fn.umsImageSlider.unlock = function(p_elem)
	{
		$(p_elem).attr('data-lock', '0');
	};

	$.fn.umsImageSlider.islocked = function(p_elem)
	{
		return ($(p_elem).attr('data-lock') === '1');
	};

	$.fn.umsImageSlider._toogleCards = function(p_elem)
	{
		var ids    = $(p_elem).attr('data-ums-image-slider-ids');
		var obj    = objects[ids];
		var client = obj.element.find('.ums-image-slider-client');
		$(client).append(client.find('div:first').detach());
	};

	$.fn.umsImageSlider.goto = function(p_elem, p_page)
	{
		var ids    = $(p_elem).attr('data-ums-image-slider-ids');
		var obj    = objects[ids];
		var client = obj.element.find('.ums-image-slider-client');

		var page = $(client).find('.ums-image-slider-slide:eq(1)').attr('data-page');
		var stack_max = (obj.options.images.length * 2);
		var k = 0;
		while(page != p_page)
		{
			$.fn.umsImageSlider._toogleCards(p_elem);
			page = $(client).find('.ums-image-slider-slide:eq(1)').attr('data-page');
			k++; if (k >= stack_max) { return false; }
		}
	};

	$.fn.umsImageSlider.next = function(p_elem)
	{
		if ( $.fn.umsImageSlider.islocked(p_elem) ) { return false; }
		$.fn.umsImageSlider.lock(p_elem);

		var ids          = $(p_elem).attr('data-ums-image-slider-ids');
		var obj          = objects[ids];
		var client       = obj.element.find('.ums-image-slider-client');
		var step         = (100 / obj.options.cols);
		var left         = parseFloat($('.ums-image-slider-client').css('left'));
		var topwidth     = parseFloat($(obj.element).width());
		var clientwidth  = parseFloat($(client).width());
		var firstElement = client.find('div:first');
		var pointer      = clientwidth - Math.floor(Math.abs(left));
		var test         = ( (pointer >= (topwidth - 5)) && (pointer <= (topwidth + 5)) );

		var first = firstElement.detach();
		client.append(first);
		$(client).animate({'left': '+=' + step + '%'}, 0);
		$(client).animate({'left': '-=' + step + '%'}, obj.options.speed, obj.options.easing, function(){ $.fn.umsImageSlider.unlock(p_elem); });
	};

	$.fn.umsImageSlider.prev = function(p_elem)
	{
		if ( $.fn.umsImageSlider.islocked(p_elem) ) { return false; }
		$.fn.umsImageSlider.lock(p_elem);

		var ids         = $(p_elem).attr('data-ums-image-slider-ids');
		var obj         = objects[ids];
		var step        = (100 / obj.options.cols);
		var client      = obj.element.find('.ums-image-slider-client');
		var left        = parseFloat($('.ums-image-slider-client').css('left'));
		var lastElement = client.find('div:last');

		var last = lastElement.detach();
		client.prepend(last);
		$(client).animate({'left': '-=' + step + '%'}, 0);
		$(client).animate({'left': '+=' + step + '%'}, obj.options.speed, obj.options.easing, function(){ $.fn.umsImageSlider.unlock(p_elem); });
	};

	$.fn.umsImageSlider.process = function(p_elem, p_options)
	{
		var ids = $.fn.umsImageSlider.makeId();
		while (objects[ids] !== undefined)
		{
			ids = $.fn.umsImageSlider.makeId();
		}
		var comp = {};
		comp.element = p_elem;
		comp.options = p_options;
		p_elem.css
		(
			{
				'position'   : 'relative',
				'box-sizing' : 'border-box',
				'width'      : '100%',
				'height'     : '100%',
			}
		);
		var divs = '';
		var div_width = (100 / p_options.images.length);
		var div_one_width = (100 / p_options.cols);

		for(var k in p_options.images)
		{
			divs += '<div class="ums-image-slider-slide slide" data-page="' + (parseInt(k)+1) + '" style="box-sizing: border-box; width: ' + div_width + '%; background-image: url(' + p_options.images[k] + '); float: left; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: cover;"></div>';
		}

		div_width = ( (100 / p_options.cols) * p_options.images.length);

		p_elem.append('<div class="ums-image-slider-stage" style="width: 100%; height: 100%; overflow: hidden; position: relative;"><div class="ums-image-slider-client" style="position: absolute; left: -' + div_one_width + '%; width: ' + div_width + '%; height: 100%;">' + divs + '</div></div>');
		if (p_options.nav.show === true)
		{
			p_elem.append('<div class="ums-image-slider-nav"><a href="#" class="btn btn-prev">' + p_options.nav.captions.prev +'</a><a href="#" class="btn btn-next">' + p_options.nav.captions.next +'</a></div>');
		}

		objects[ids] = (comp);
		p_elem.attr('data-ums-image-slider-ids', ids).addClass('ImageSlider');

		$(p_elem).on
		(
			'swipeleft',
			function(e)
			{
				$.fn.umsImageSlider.next(p_elem);
			}
		);

		$(p_elem).on
		(
			'swiperight',
			function(e)
			{
				$.fn.umsImageSlider.prev(p_elem);
			}
		);


		$(document).on
		(
			'click',
			'#' + $(p_elem).attr('id') + ' .ums-image-slider-nav .btn-next',
			function(e)
			{
				$.fn.umsImageSlider.next($(p_elem));
				e.preventDefault();
			}
		);

		$(document).on
		(
			'click',
			'#' + $(p_elem).attr('id') + ' .ums-image-slider-nav .btn-prev',
			function(e)
			{
				$.fn.umsImageSlider.prev($(p_elem));
				e.preventDefault();
			}
		);

		var client = $(p_elem).find('.ums-image-slider-client');
		$(client).prepend(client.find('div:last').detach());
	};

})( jQuery );