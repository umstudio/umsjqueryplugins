# UMS Image Slider

Plugin para exibir imagens em galeria.

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Estrutura Básica (HTML)
```html
<script type="text/javascript" src="../dist/jqueryUmsImageSlider.min.js"></script>
```
### Chamada Plugin

```js
$('#gallery01').umsImageSlider
(
    {
        'images':
        [
            'http://placehold.it/350x150&text=1',
            'http://placehold.it/350x150&text=2',
            'http://placehold.it/350x150&text=3',
            'http://placehold.it/350x150&text=4',
            'http://placehold.it/350x150&text=5',
        ],
        'cols'  : 3,
        'easing': 'easeOutExpo',
        'speed' : 500
    }
);
```

### Dependências
* [jQuery]


[jQuery]:http://code.jquery.com/