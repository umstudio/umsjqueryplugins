// Last modified: 2017-10-31 16:51:00
(function($)
{
	$.fn.___UmsTextMeterGetSize = function()
	{
		var _style = window.getComputedStyle(this[0]);
		return {
			'width' : parseInt(_style.width),
			'height': parseInt(_style.height)
		}
	};
}(jQuery));

(function($)
{
	$.fn.___UmsTextMeter = function()
	{
		var $reference;
		var appended   = false;
		var $self      = $(this);
		var size       = { 'width': 0, 'height': 0 };
		var div_ref_id = 'ums-text-meter-' + $self.attr('data-textmeterid');

		var verifyMeters = function()
		{
			var div_html = $self.html();
			var ref_html = $reference.html();
			if (div_html != ref_html)
			{
				$reference.html(div_html);
			}

			var div_size = $self.___UmsTextMeterGetSize();
			var ref_size = $reference.___UmsTextMeterGetSize();

			var lines = Math.floor(div_size.height / ref_size.height);

			var old_lines = $self.attr('meter-lines');
			if (old_lines != lines)
			{
				$self.attr('meter-lines', lines);
				var obj = 
				{
					'id'    : $self.attr('id'),
					'obj'   : $self,
					'lines' : lines,
				};
				$(document).trigger('onUmsTextMeterChange', obj);
			}
		};

		var monitorWidth = function()
		{
			var curr_size = $self.___UmsTextMeterGetSize();
			if (curr_size.width != size.width || curr_size.height != size.height)
			{
				size = curr_size;
				verifyMeters();
			}

			setTimeout(function() { monitorWidth(); }, 250 );
		};

		if (!appended)
		{
			appended = true;
			$('#ums-text-meter-main').append('<div id="' + div_ref_id + '" style="position: absolute; opacity: 0; visibility: hidden; top: 0px; left: 0px; height: auto; width: auto; white-space: nowrap;">Reference.</div>');
			$reference = $('#' + div_ref_id);
			$reference.html(this.html());
		}

		var div_style = window.getComputedStyle($self[0]);
		$reference.css('fontSize', div_style.fontSize);
		$reference.css('fontFamily', div_style.fontFamily);
		
		setTimeout(function() { monitorWidth(); }, 250 );

		return this;
	};

}(jQuery));

(function($)
{
	$.fn.UmsTextMeter = function()
	{
		var makeId = function()
		{
			var text = "";
			var possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789";
			var pl = possible.length;
			for(var i = 0; i < 8; i++)
			{
				text += possible.charAt(Math.floor(Math.random() * pl));
			}
			return text;
		};

		if ($('#ums-text-meter-main').length <= 0)
		{
			$('body').append('<div id="ums-text-meter-main"></div>');
		}

		var result = this.each(function()
		{
			var ids = makeId();
			$(this).attr('data-textmeterid', ids);
			$(this).___UmsTextMeter();
		});

		return result;
	};
}(jQuery));