# UMS Text Meter
Plugin para mostrar quantas linhas de texto uma div possui.

### Demo
<http://umstudiohomolog.com.br/ums/jquery-plugins/jqueryUmsTextMeter/demo/>

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Estrutura Básica (HTML)
```html
<script type="text/javascript" src="js/lib/jqueryTextMeter.min.js"></script>
```
### Chamada Plugin

```html
    <div id="label-example">&nbsp;</div>
    <div id="example" class="dynamic-text">
        <p>
            Example: Este é um teste para vermos se isto funciona realmente. Este é um teste para vermos se isto funciona realmente. Este é um teste para vermos se isto funciona realmente.
        </p>
    </div>
    <div id="label-another">&nbsp;</div>
    <div id="another" class="dynamic-text">
        <p>
            Another: Este é um teste para vermos se isto funciona realmente. Este é um teste para vermos se isto funciona realmente. Este é um teste para vermos se isto funciona realmente. Este é um teste para vermos se isto funciona realmente. Este é um teste para vermos se isto funciona realmente. Este é um teste para vermos se isto funciona realmente.
        </p>
    </div>
```

```js
$(document).ready
(
    function()
    {
        $('.dynamic-text > p').UmsTextMeter();

        $(document).on
        (
            'onUmsTextMeterChange',
            function(event, result)
            {
                var $obj = result.obj;
                var ids = $obj.parent().attr('id');
                $('#label-' + ids).html( result.lines + ' Linhas' );
            }
        );
    }
);
```

### Triggers
```js
$(document).on('onUmsTextMeterChange' , function(e, obj) { console.log(obj); } );
```
### Dependências
* [jQuery]

[jQuery]:http://code.jquery.com/