$(document).ready
(
	function()
	{
		$('.dynamic-text > p').UmsTextMeter();

		$(document).on
		(
			'onUmsTextMeterChange',
			function(event, result)
			{
				var $obj = result.obj;
				var ids = $obj.parent().attr('id');
				$('#label-' + ids).html( result.lines + ' Linhas' );
			}
		);
	}
);