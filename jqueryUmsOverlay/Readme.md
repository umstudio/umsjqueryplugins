# UMS Overlay

Plugin para exibir e remover overlay.

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Estrutura Básica (HTML)
```html
<script type="text/javascript" src="js/jqueryUmsOverlay.min.js"></script>
```
### Chamada Plugin

```js
$(document).umsOverlay
(
    {
        'color'        : 'green',
        'opacity'      : 0.6,
        'zindex'       : 99999,
        'speed'        : 250,
        'closeOnClick' : true,
        'fireTriggers' : true,
        'events':
        {
            'onBeforeShow': function()
            {
                console.log('onBeforeShow');
            },
            'onAfterShow': function()
            {
                console.log('onAfterShow');
            },
            'onBeforeClose': function()
            {
                console.log('onBeforeClose');
            },
            'onAfterClose': function()
            {
                console.log('onAfterClose');
            }
        }
    }
);
```

### Métodos
```js
$(document).umsOverlay('show');
$(document).umsOverlay('close');
```
### Triggers
```js
$(document).on('umsOverlayOnBeforeShow' , function(e) { console.log('Trigger onBeforeShow' ); } );
$(document).on('umsOverlayOnAfterShow'  , function(e) { console.log('Trigger onAfterShow'  ); } );
$(document).on('umsOverlayOnBeforeClose', function(e) { console.log('Trigger onBeforeClose'); } );
$(document).on('umsOverlayOnAfterClose' , function(e) { console.log('Trigger onAfterClose' ); } );
```
### Dependências
* [jQuery]


[jQuery]:http://code.jquery.com/