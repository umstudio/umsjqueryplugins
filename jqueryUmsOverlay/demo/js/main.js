$(document).ready(function() {

	$(document).umsOverlay
	(
		{
			'color'        : '#008000',
			'opacity'      : 0.6,
			'zindex'       : 99999,
			'speed'        : 500,
			'closeOnClick' : true,
			'fireTriggers' : true,
			events:
			{
				'onBeforeShow': function()
				{
					console.log('onBeforeShow');
				},
				'onAfterShow': function()
				{
					console.log('onAfterShow');
				},
				'onBeforeClose': function()
				{
					console.log('onBeforeClose');
				},
				'onAfterClose': function()
				{
					console.log('onAfterClose');
				}
			}
		}
	);

	$(document).umsOverlay
	(
		{
			'id'           : 'black',
			'color'        : '#000000',
			'opacity'      : 0.7,
			'zindex'       : 99999,
			'speed'        : 500,
			'closeOnClick' : true,
			'fireTriggers' : true,
			events:
			{
				'onBeforeShow': function()
				{
					console.log('onBeforeShow - Black');
				},
				'onAfterShow': function()
				{
					console.log('onAfterShow - Black');
				},
				'onBeforeClose': function()
				{
					console.log('onBeforeClose - Black');
				},
				'onAfterClose': function()
				{
					console.log('onAfterClose - Black');
				}
			}
		}
	);

	$(document).umsOverlay
	(
		{
			'id'           : 'wait',
			'color'        : '#000000',
			'opacity'      : 0.7,
			'zindex'       : 99999,
			'speed'        : 500,
			'closeOnClick' : true,
			'fireTriggers' : true,
			events:
			{
				'onBeforeShow': function()
				{
					console.log('onBeforeShow - Black');
				},
				'onAfterShow': function()
				{
					console.log('onAfterShow - Black');
				},
				'onBeforeClose': function()
				{
					console.log('onBeforeClose - Black');
				},
				'onAfterClose': function()
				{
					console.log('onAfterClose - Black');
				}
			}
		}
	);

	$(document).on('umsOverlayOnBeforeShow' , function(e, options) { console.log('Trigger onBeforeShow: '  + options.id); } );
	$(document).on('umsOverlayOnAfterShow'  , function(e, options) { console.log('Trigger onAfterShow: '   + options.id); } );
	$(document).on('umsOverlayOnBeforeClose', function(e, options) { console.log('Trigger onBeforeClose: ' + options.id); } );
	$(document).on('umsOverlayOnAfterClose' , function(e, options) { console.log('Trigger onAfterClose: '  + options.id); } );

	$(document).on
	(
		'click',
		'#btn-show-1',
		function(e)
		{
			e.preventDefault();
			$(document).umsOverlay('show');
		}
	);

	$(document).on
	(
		'click',
		'#btn-show-2',
		function(e)
		{
			e.preventDefault();
			$(document).umsOverlay
			(
				'show',
				{
					'id': 'black',
					'onBeforeShow': function()
					{
						console.log('BeforeShow Black Callback');
					},
					'onShow': function()
					{
						console.log('Show Black Callback');
					},
					'onBeforeClose': function()
					{
						console.log('BeforeClose Black Callback');
					},
					'onClose': function()
					{
						console.log('Close Black Callback');
					}
				}
			);
		}
	);

	$(document).on
	(
		'click',
		'#btn-show-3',
		function(e)
		{
			e.preventDefault();
			$(document).umsOverlay
			(
				'wait',
				{
					'id': 'wait',
					'ico': 'img/wait-02.gif',
					'onShow': function()
					{
						setTimeout
						(
							function()
							{
								$(document).umsOverlay('close', {'id': 'wait'});
							},
							2000
						);
					}
				}
			);
		}
	);

	// $(document).umsOverlay('close');
	// $(document).umsOverlay('close', {'id': 'black'});

});