<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>jqueryUmsOverlay - Demo</title>
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="../dist/jqueryUmsOverlay.min.js"></script>
	<!-- <script type="text/javascript" src="../src/jqueryUmsOverlay.js"></script> -->
	<script type="text/javascript" src="js/main.js"></script>
</head>
<body>
	
	<button id="btn-show-1">Show Green Overlay</button>
	<button id="btn-show-2">Show Black Overlay</button>
	<button id="btn-show-3">Show Wait Overlay</button>

	<?php for($k = 0; $k < 5; $k++) { ?>
		<p>
			<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus fuga sequi accusamus, quidem, officiis vitae eum minima aliquam mollitia! Impedit hic, optio quo consequuntur cupiditate corporis nulla, provident enim odio!</span>
			<span>Iusto quos, sunt neque quasi excepturi laborum illo, quod debitis doloribus nisi repellendus nobis reiciendis asperiores, expedita autem doloremque aspernatur modi dolorem quisquam eum inventore! Perspiciatis facere deserunt, enim molestias.</span>
			<span>Fugiat sed tempore officiis reiciendis quidem recusandae, error, quia ab dolor ullam beatae accusamus inventore! Nesciunt perferendis, omnis quae inventore numquam placeat vel possimus excepturi deserunt, illum eos repudiandae ducimus.</span>
			<span>Eligendi natus eveniet aliquam. Sed quaerat aut obcaecati voluptates adipisci iusto minus, fugit, sequi deserunt sunt facere ab modi magnam consectetur, eligendi molestiae dolorem! Alias delectus, saepe at eaque officiis.</span>
		</p>
	<?php } ?>

</body>
</html>