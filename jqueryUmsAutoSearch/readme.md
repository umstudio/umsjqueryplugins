# jQuery Ums Auto Search
> Plugin jQuery para efetuar auto buscas em tempo de digitação.

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Demo
<http://umstudiohomolog.com.br/ums/jquery-plugins/jqueryUmsAutoSearch/demo/>

### Estrutura Básica (HTML)
```html
<script type="text/javascript" src="../dist/jqueryUmsAutoSearch.min.js"></script>
```
### Exemplo de uso
```html
<h1>jQuery Ums Auto Search</h1>
<form id="frmSearch">
    <label for="frBusca">Texto de Busca</label>
    <br>
    <input type="text" name="frBusca" id="frBusca" value="" placeholder="Texto de Busca" size="50">
</form>
<hr/>
<button id="btn-reset">Limpar</button>
```
```js
$(document).ready(function() {
    $('#frBusca').auto_search
    (
        {
            'interval_ms' : 1250,
            'min_length'  : 3,
            'callback'    : function(p_args)
            {
                $('#buscas').append('<li>' + p_args.value + '</li>');
            }
        }
    );
    
    $(document).on
    (
        'click',
        '#btn-reset',
        function(e)
        {
            e.preventDefault();
            $('#frBusca').auto_search('reset');
            $('#buscas li').remove();
        }
    );
});
```