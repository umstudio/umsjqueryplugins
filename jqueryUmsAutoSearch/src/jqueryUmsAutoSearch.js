(function($){

	var defaults = 
	{
		'interval_ms' : 1250,
		'min_length' : 3,
		'callback' : function(){ console.log('trigger search'); }
	};

	var options = {};
	var objects = [];

	var methods =
	{
		init : function(p_options)
		{
			options = $.extend(true, {}, defaults, p_options);

			var result = this.each(function()
			{
				if (typeof $(this).attr('id') !== 'string')
				{
					$.error('The element needs attribute ID');
				}
				$.fn.auto_search.process($(this), options);
			});

			return result;
		},
		reset: function() { return this.each(function() { $.fn.auto_search.reset($(this)); }); }
	};

	$.fn.auto_search = function(methodOrOptions)
	{
		if ( methods[methodOrOptions] )
		{
			return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof methodOrOptions === 'object' || ! methodOrOptions )
		{
			return methods.init.apply( this, arguments );
		}
		else
		{
			$.error('Method ' + methodOrOptions + ' does not exist on jQuery.auto_search');
		}
	};

	$.fn.auto_search.makeId = function()
	{
		var text = "";
		var possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789";
		var pl = possible.length;
		for(var i = 0; i < 8; i++)
		{
			text += possible.charAt(Math.floor(Math.random() * pl));
		}
		return text;
	};

	$.fn.auto_search.addUniqueId = function(p_elem)
	{
		var ids = $.fn.auto_search.makeId();
		while (objects[ids] !== undefined)
		{
			ids = $.fn.auto_search.makeId();
		}
		p_elem.attr('data-ums-modal-ids', ids);
		return ids;
	};

	$.fn.auto_search.process = function(p_elem, p_options)
	{
		var ids = $.fn.auto_search.addUniqueId(p_elem);

		var comp = {};
		comp.id          = ids;
		comp.element     = p_elem;
		comp.options     = p_options;
		comp.timer       = null;
		comp.old_var     = '';
		comp.last_search = '';
		comp.history     = [];
		objects[ids]     = (comp);

		$(document).on
		(
			'keydown',
			'#' + p_elem.attr('id'),
			function(e)
			{
				var ids = $(this).attr('data-ums-modal-ids');
				var obj = objects[ids];
				if (obj.timer !== null)
				{
					clearTimeout(obj.timer);
					obj.timer = null;
				}
			}
		);

		$(document).on
		(
			'keyup',
			'#' + p_elem.attr('id'),
			function(e)
			{
				var ids = $(this).attr('data-ums-modal-ids');
				var obj = objects[ids];

				var value = $(this).val();
				if (e.keyCode === 13)
				{
					$.fn.auto_search.execute_callback(obj.element);
				}
				else
				{
					if ( (value === obj.old_var) && (value.trim() === '') )
					{
						if (value.trim() === '')
						{
							obj.old_var = '';
						}
						return false;
					}
					
					if (value.length < obj.options.min_length)
					{
						return false;
					}
					obj.old_var = value;
		
					obj.timer = setTimeout
					(
						function()
						{
							$.fn.auto_search.execute_callback(obj.element);
						},
						obj.options.interval_ms
					);
				}
			}
		);
	};

	$.fn.auto_search.execute_callback = function(p_elem)
	{
		var ids = p_elem.attr('data-ums-modal-ids');
		var obj = objects[ids];

		var value = obj.element.val();
		if (value !== obj.last_search)
		{
			obj.history.push(value);
			obj.last_search = value;
			obj.options.callback
			(
				{
					'value'   : value,
					'element' : obj.element,
					'options' :
					{
						'interval_ms': obj.options.interval_ms,
						'min_length': obj.options.min_length,
					}
				}
			);
		}
	};

	$.fn.auto_search.reset = function(p_elem)
	{
		var ids = p_elem.attr('data-ums-modal-ids');
		var obj = objects[ids];

		obj.old_var     = '';
		obj.last_search = '';

		if (obj.timer !== null)
		{
			clearTimeout(obj.timer);
			obj.timer = null;
		}

		obj.element.val('');
	};

})( jQuery );