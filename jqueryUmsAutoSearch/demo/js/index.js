/* global CjsBaseClass,CJS_DEBUG_MODE_0,CJS_DEBUG_MODE_1,CJS_DEBUG_MODE_2 */
var umsapp = umsapp || {};
umsapp.Tdemoclass = function($, objname, options)
{
	'use strict';
	var self = this;

	this.init = function()
	{
		self.initVars();
		self.processTriggers();
		self.onElementsEvents();
	};

	this.initVars = function()
	{
		
	};

	this.onReady = function()
	{
		// CODE ON APLICATION IS READY
		self.addInputAutoSearch();
		self.start();
	};

	this.start = function()
	{
		// CODE ON APLICATION IS STARTED
		self.triggerStarted();
	};

	this.processTriggers = function()
	{

	};

	this.onElementsEvents = function()
	{
		$(document).on
		(
			'submit',
			'#frmSearch',
			function(e)
			{
				e.preventDefault();
				
			}
		);

		$(document).on
		(
			'click',
			'#btn-reset',
			function(e)
			{
				e.preventDefault();
				$('#frBusca').auto_search('reset');
				$('#buscas li').remove();
			}
		);
	};

	this.addInputAutoSearch = function()
	{
		$('#frBusca').auto_search
		(
			{
				'interval_ms' : 1250,
				'min_length'  : 3,
				'callback'    : function(p_args)
				{
					self.log.alert('Execute Search Triggered: "' + p_args.value + '"', p_args);
					$('#buscas').append('<li>' + p_args.value + '</li>');
				}
			}
		)
		.select();
	};

	CjsBaseClass.call(this, $, objname, options);

	this.init();
};

umsapp.democlass = new umsapp.Tdemoclass
(
	window.cjsbaseclass_jquery,
	'democlass',
	{
		'debug'         : CJS_DEBUG_MODE_1,
		'highlighted'   : 'auto',
		'custom_option' : 'custom_value'
	}
);