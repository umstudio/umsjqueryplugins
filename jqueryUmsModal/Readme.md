# UMS Modal

Plugin para exibir e remover Modal Dialogs.

### Compatível
    * Google Chrome
    * Firefox
    * Safari
    * Internet Explorer (9+)

### Estrutura Básica (HTML)
```html
<script type="text/javascript" src="js/lib/jqueryUmsOverlay.min.js"></script>
<script type="text/javascript" src="js/lib/jqueryUmsBreakPoints.min.js"></script>
<script type="text/javascript" src="js/lib/jqueryUmsModal.min.js"></script>
```
### Chamada Plugin

```js
$('#modal-1').umsModal
(
    {
        {
            'closebutton' : 'default', // custom or none
            'fireTriggers'  : true,
            'debug' : true,
            'onReady' : function()
            {
                console.log('Ums Modal Ready');
            },
            'overlay' :
            {
                'color'        : '#000000',
                'opacity'      : 0.6,
                'zindex'       : 200,
                'speed'        : 250,
                'closeOnClick' : true,
                'fireTriggers' : true,
            },
            'frontend':
            {
                'width' : 720,
                'height' : 530,
                'valign' : 'middle',
            },
            'customstyles': 
            {
                'widths': [400,600,800],
                'styles': ['lt400','b400a600','b600a800','gt800']
            },
            'events':
            {
                'onBeforeShow': function()
                {
                    console.log('USER FUNCTION CALLBACK ONBEFORESHOW');
                },
                'onAfterShow': function()
                {
                    console.log('USER FUNCTION CALLBACK ONAFTERSHOW');
                },
                'onBeforeClose': function()
                {
                    console.log('USER FUNCTION CALLBACK ONBEFORECLOSE');
                },
                'onAfterClose': function()
                {
                    console.log('USER FUNCTION CALLBACK ONAFTERCLOSE');
                },
                'onReady' : function()
                {
                    console.log('Ums Modal - MODAL-2 - Ready');
                },
            },
        }
    }
);

$(document).on
(
    'click',
    '#btn-show',
    function(e)
    {
        e.preventDefault();
        $('#modal-1').umsModal('show');
    }
);
```

### Métodos
```js
$('#modal-1').umsModal('show');
$('#modal-1').umsModal('close');
```
### Triggers
```js
$(document).on('umsModalOnBeforeShow' , function(e) { console.log('Trigger onBeforeShow' ); } );
$(document).on('umsModalOnAfterShow'  , function(e) { console.log('Trigger onAfterShow'  ); } );
$(document).on('umsModalOnBeforeClose', function(e) { console.log('Trigger onBeforeClose'); } );
$(document).on('umsModalOnAfterClose' , function(e) { console.log('Trigger onAfterClose' ); } );
```
### Dependências
* [jQuery]
* [jqueryUmsOverlay]
* [jqueryUmsBreakPoints]


[jQuery]:http://code.jquery.com/
[jqueryUmsOverlay]:https://bitbucket.org/umstudio/umsjqueryplugins/src/a859780811dc9ac65dd485365afed5b5bdcd2be6/jqueryUmsOverlay/?at=master
[jqueryUmsBreakPoints]:https://bitbucket.org/umstudio/umsjqueryplugins/src/a859780811dc9ac65dd485365afed5b5bdcd2be6/jqueryUmsBreakPoints/?at=master