<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>jqueryUmsModal - Demo</title>
	<link rel="stylesheet" type="text/css" href="../dist/jqueryUmsModal.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="../../jqueryUmsOverlay/dist/jqueryUmsOverlay.min.js"></script>
	<script type="text/javascript" src="../../jqueryUmsBreakPoints/dist/jqueryUmsBreakPoints.min.js"></script>
	<script type="text/javascript" src="../dist/jqueryUmsModal.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</head>
<body>
	<button id="btn-show-1">Show Modal 1</button>
	<button id="btn-show-2">Show Modal 2</button>

	<?php for($k = 0; $k < 5; $k++) { ?>
		<p>
			<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus fuga sequi accusamus, quidem, officiis vitae eum minima aliquam mollitia! Impedit hic, optio quo consequuntur cupiditate corporis nulla, provident enim odio!</span>
			<span>Iusto quos, sunt neque quasi excepturi laborum illo, quod debitis doloribus nisi repellendus nobis reiciendis asperiores, expedita autem doloremque aspernatur modi dolorem quisquam eum inventore! Perspiciatis facere deserunt, enim molestias.</span>
			<span>Fugiat sed tempore officiis reiciendis quidem recusandae, error, quia ab dolor ullam beatae accusamus inventore! Nesciunt perferendis, omnis quae inventore numquam placeat vel possimus excepturi deserunt, illum eos repudiandae ducimus.</span>
			<span>Eligendi natus eveniet aliquam. Sed quaerat aut obcaecati voluptates adipisci iusto minus, fugit, sequi deserunt sunt facere ab modi magnam consectetur, eligendi molestiae dolorem! Alias delectus, saepe at eaque officiis.</span>
		</p>
	<?php } ?>

	<div id="modal-1" class="modal-1">
		<h1>Modal 1</h1>
		<?php for($k = 0; $k < 4; $k++) { ?>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum sapiente est dicta ea quos deserunt quas, excepturi neque velit. Exercitationem quod, ad iure? Earum, et. Cum voluptates quae velit sed!
		<?php } ?>
	</div>

	<div id="modal-2" class="modal-2">
		<h1>Modal 2</h1>
		<?php for($k = 0; $k < 4; $k++) { ?>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum sapiente est dicta ea quos deserunt quas, excepturi neque velit. Exercitationem quod, ad iure? Earum, et. Cum voluptates quae velit sed!
		<?php } ?>
	</div>
</body>
</html>