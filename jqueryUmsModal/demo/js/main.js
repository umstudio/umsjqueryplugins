$(document).ready(function() {

	$('#modal-1').umsModal
	(
		{
			'closebutton' : 'default', // custom or none
			'debug' : true,
			'fireTriggers': true,
			'overlay' :
			{
				'color'        : '#000000',
				'opacity'      : 0.6,
				'zindex'       : 200,
				'speed'        : 250,
				'closeOnClick' : true,
				'fireTriggers' : true,
			},
			'frontend':
			{
				'width' : 720,
				'height' : 530,
				'valign' : 'middle',
			},
			'customstyles': 
			{
				'widths': [400,600,800],
				'styles': ['lt400','b400a600','b600a800','gt800']
			},
			'events':
			{
				'onBeforeShow': function()
				{
					console.log('MAIN.JS ONBEFORESHOW');
				},
				'onAfterShow': function()
				{
					console.log('MAIN.JS ONAFTERSHOW');
				},
				'onBeforeClose': function()
				{
					console.log('MAIN.JS ONBEFORECLOSE');
				},
				'onAfterClose': function()
				{
					console.log('MAIN.JS ONAFTERCLOSE');
				},
				'onReady' : function()
				{
					console.log('Ums Modal - MODAL-1 - Ready');
				},
			}
		}
	);

	$(document).on
	(
		'click',
		'#btn-show-1',
		function(e)
		{
			e.preventDefault();
			$('#modal-1').umsModal('show');
		}
	);

	$('#modal-2').umsModal
	(
		{
			'closebutton' : 'default', // custom or none
			'debug' : true,
			'fireTriggers': true,
			'overlay' :
			{
				'color'        : '#345EA9',
				'opacity'      : 0.6,
				'zindex'       : 200,
				'speed'        : 250,
				'closeOnClick' : true,
				'fireTriggers' : true,
			},
			'frontend':
			{
				'width' : 720,
				'height' : 530,
				'valign' : 'middle',
			},
			'customstyles': 
			{
				'widths': [400,600,800],
				'styles': ['lt400','b400a600','b600a800','gt800']
			},
			'events':
			{
				'onReady' : function()
				{
					console.log('Ums Modal - MODAL-2 - Ready');
				},
			}
		}
	);

	$(document).on
	(
		'click',
		'#btn-show-2',
		function(e)
		{
			e.preventDefault();
			$('#modal-2').umsModal('show');
		}
	);

});