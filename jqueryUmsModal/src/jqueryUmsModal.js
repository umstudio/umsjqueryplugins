(function($){

	var defaults =
	{
		'id'            : 'ums-modal',
		'class'         : 'ums-modal',
		'closebutton'   : 'default', // none, default, or custom
		'frontend':
		{
			'width'  : 300,
			'height' : 'auto',  // 300
			'valign' : undefined, // 'top', 'middle' and 'bottom'
		},
		'padding'       : '20',
		'customstyles'  : 
		{
			'widths': [],
			'styles': []
		},
		'debug'         : false,
		'zindex'        : 900,
		'speed'         : 0,
		'fireTriggers'  : true,
		'events':
		{
			'onBeforeShow'  : null,
			'onAfterShow'   : null,
			'onBeforeClose' : null,
			'onAfterClose'  : null,
			'onReady'       : null,
		},
		'overlay'       : false
	};
	var options = {};
	var objects = [];

	var methods =
	{
		init : function(p_options)
		{
			options = $.extend(true, {}, defaults, p_options);

			var result = this.each(function()
			{
				if (typeof $(this).attr('id') !== 'string')
				{
					$.error('The element needs attribute ID');
				}
				$.fn.umsModal.process($(this), options);
			});

			return result;
		},
		show: function() { return this.each(function() { $.fn.umsModal.show(this); }); },
		close: function() { return this.each(function() { $.fn.umsModal.close(this); }); },
	};

	$.fn.umsModal = function(methodOrOptions)
	{
		if ( methods[methodOrOptions] )
		{
			return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof methodOrOptions === 'object' || ! methodOrOptions )
		{
			return methods.init.apply( this, arguments );
		}
		else
		{
			$.error('Method ' + methodOrOptions + ' does not exist on jQuery.umsModal');
		}
	};

	$.fn.umsModal.trigger = function(p_trigger_name, p_obj)
	{
		if (p_obj.options.debug === true)
		{
			console.log(p_trigger_name, p_obj);
		}
		$(document).trigger(p_trigger_name, p_obj);
	};

	$.fn.umsModal.show = function(p_elem)
	{
		var ids = $(p_elem).attr('data-ums-modal-ids');
		var obj = objects[ids];

		if (obj.options.events.onBeforeShow  !== null)
		{
			obj.options.events.onBeforeShow();
		}

		if (obj.options.fireTriggers !== false)
		{
			$.fn.umsModal.trigger('umsModalOnBeforeShow', obj);
		}

		var _show = function()
		{
			$('#' + obj.id).css('opacity', 0).show(0);
			$('#' + obj.id).animate
			(
				{
					'opacity': 1
				},
				obj.options.speed,
				function()
				{
					if (obj.options.events.onAfterShow !== null)
					{
						obj.options.events.onAfterShow();
					}
					if (obj.options.fireTriggers !== false)
					{
						$.fn.umsModal.trigger('umsModalOnAfterShow', obj);
					}
				}
			);
		};

		if (obj.options.overlay !== false)
		{
			$(document).umsOverlay
			(
				'show',
				{
					'id': obj.id,
					'onShow': function() { _show(); } ,
					'events':
					{
						'onBeforeClose': function()
						{
							$.fn.umsModal.close(p_elem);
						}
					}
				}
			);
		}
		else
		{
			_show();
		}
	};

	$.fn.umsModal.close = function(p_elem)
	{
		var modal_element = $(p_elem).parents('.ums-modal');
		var ids = $(p_elem).attr('data-ums-modal-ids');
		var obj = objects[ids];

		if (obj.options.events.onBeforeClose !== null)
		{
			obj.options.events.onBeforeClose();
		}

		if (obj.options.fireTriggers !== false)
		{
			$.fn.umsModal.trigger('umsModalOnBeforeClose', obj);
		}

		$(modal_element).animate
		(
			{
				'opacity': 0
			},
			obj.options.speed,
			function()
			{
				$(modal_element).hide(0);

				if (obj.options.overlay !== false)
				{
					$(document).umsOverlay
					(
						'close',
						{
							'id': obj.options.overlay.id
						}
					);
				}

				if (obj.options.events.onAfterClose !== null)
				{
					obj.options.events.onAfterClose();
				}
				if (obj.options.fireTriggers !== false)
				{
					$.fn.umsModal.trigger('umsModalOnAfterClose', obj);
				}
			}
		);
	};

	$.fn.umsModal.makeId = function()
	{
		var text = "";
		var possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789";
		var pl = possible.length;
		for(var i = 0; i < 8; i++)
		{
			text += possible.charAt(Math.floor(Math.random() * pl));
		}
		return text;
	};

	$.fn.umsModal.addUniqueId = function(p_elem)
	{
		var ids = $.fn.umsModal.makeId();
		while (objects[ids] !== undefined)
		{
			ids = $.fn.umsModal.makeId();
		}
		p_elem.attr('data-ums-modal-ids', ids);
		return ids;
	};

	$.fn.umsModal.process = function(p_elem, p_options)
	{
		var ids = $.fn.umsModal.addUniqueId(p_elem);

		var element_class = p_elem.attr('class');
		element_class = (element_class === undefined) ? 'ums-modal' : element_class + '-ums-modal';

		var element_addclass = p_elem.attr('id');
		element_addclass = (element_addclass === undefined) ? '' : ' ' + element_addclass + '-ums-modal';

		if(element_class.trim() == element_addclass.trim())
		{
			element_addclass = '';
		}

		var modalstr = '<div id="' + ids + '" class="' + element_class + element_addclass + ' ums-modal" style="display: none"><div class="wrapper"></div></div>';
		$(p_elem).wrap(modalstr);

		var $p_original = $('[data-ums-modal-ids="' + ids + '"]');

		if ( (p_options.closebutton == 'default') || (p_options.closebutton == 'custom') )
		{
			var button_class = 'ums-modal-btn-close-' + p_options.closebutton;
			$('#' + ids).append('<div class="ums-modal-btn-close ' + button_class + '"></div>');
		}

		var objModal = $('#' + ids);
		objModal.css
		(
			{
				'position'    : 'absolute',
				'width'       : p_options.frontend.width,
				'height'      : p_options.frontend.height,
				'left'        : '50%',
				'margin-left' : ((p_options.frontend.width / 2) * -1),
				'z-index'     : p_options.zindex
			}
		);

		switch(p_options.frontend.valign)
		{
			case 'top':
				objModal.css({
					'top' : '0',
					'margin-top' : '0'
				});
			break;
			case 'middle':
				objModal.css({
					'top' : '50%',
					'margin-top' : ((p_options.frontend.height / 2) * -1)
				});
			break;
			case 'bottom':
				objModal.css({
					'bottom' : '0'
				});
			break;
			default:
				if (p_options.frontend.valign !== undefined)
				{
					objModal.css(p_options.frontend.valign);
				}
			break;
		}

		if (p_options.overlay !== false)
		{
			p_options.overlay.insideelement = $('#' + ids);
			p_options.overlay.id = ids;
			$(document).umsOverlay(p_options.overlay);
		}

		var comp = {};
		comp.id      = ids;
		comp.element = p_elem;
		comp.eclass  = element_class;
		comp.options = p_options;
		objects[ids] = (comp);

		if (p_options.customstyles.widths.length > 0)
		{
			$('#' + ids).umsBreakPoints
			(
				{
					'syncWidthMediaQuery' : true,
					'widths'              : p_options.customstyles.widths,
					'onBreakPoint'        : function(p_eventdata)
					{
						var $element = $('#' + ids);
						var inside_style;
						var original_display;

						if (p_options.customstyles.styles.indexOf(p_eventdata.class_name) >= 0)
						{
							inside_style = $element.attr('style');
							original_display = $element.css('display');
							$element.removeAttr('style').attr('data-style', inside_style).css('display', original_display);
						}
						else
						{
							if ($element.attr('data-style') !== undefined)
							{
								inside_style = $element.attr('data-style');
								if (inside_style.indexOf('display: none;') >= 0)
								{
									inside_style = inside_style.replace('display: none;', '');
								}
								$element.removeAttr('data-style').attr('style', inside_style);
							}
						}
					}
				}
			);
		}

		if ( (p_options.closebutton == 'default') || (p_options.closebutton == 'custom') )
		{
			$(document).on
			(
				'tap',
				'#' + comp.id + ' .ums-modal-btn-close',
				function(e)
				{
					e.preventDefault();
					$(comp.element).umsModal('close');
				}
			);
		}

		if (p_options.events.onReady !== null)
		{
			p_options.events.onReady();
		}
	};

})( jQuery );