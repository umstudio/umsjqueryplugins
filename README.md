# README #

jQuery Plugins para utilização nos jobs da umstudio.

### Plugins Disponíveis ###

* Youtube Custom Player ([ytvcustomplayer](https://bitbucket.org/umstudio/umsjqueryplugins/src/f8bed08887e4ed3aa058c50dd7bde67b69654d0a/ytvcustomplayer/?at=master))
* Browser Width Break Points Events ([jqueryUmsBreakPoints](https://bitbucket.org/umstudio/umsjqueryplugins/src/f8bed08887e4ed3aa058c50dd7bde67b69654d0a/jqueryUmsBreakPoints/?at=master))
* Sections Observer ([jqueryUmsSectionObserver](https://bitbucket.org/umstudio/umsjqueryplugins/src/9826dcd80f778b54cabd968f33fca9e151fca257/jqueryUmsSectionObserver/?at=master))
* Enter As Alpha ([jqueryUmsEnterAsAlpha](https://bitbucket.org/umstudio/umsjqueryplugins/src/f51914d760e7772966720e0e244753a652f6e466/jqueryUmsEnterAsAlpha/?at=master))
* Ums Mustache jQuery Wrapper ([jqueryUmsMustache](https://bitbucket.org/umstudio/umsjqueryplugins/src/ba98d5845ecc799cb156814c8ec4a9d4a2c11b2d/jqueryUmsMustache/?at=master))
* Ums Text Meter ([jqueryUmsTextMeter](https://bitbucket.org/umstudio/umsjqueryplugins/src/b07e3c91362d91edcaea6286de52ebe311739449/jqueryUmsTextMeter/?at=master))