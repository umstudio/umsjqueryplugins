var gulp        = require('gulp');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');
var rename      = require('gulp-rename');
var sourcemaps  = require('gulp-sourcemaps');
var del         = require('del');
var beautify    = require('gulp-beautify');
var edit        = require('gulp-edit');
var minifyCss   = require('gulp-minify-css');
var cssbeautify = require('gulp-cssbeautify');
var gutil       = require('gulp-util');
var ftp         = require('vinyl-ftp');
var replace     = require('gulp-replace');
var date        = require('date-and-time');

var date_now = date.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

gulp.task('cleanJqueryUmsOverlayDistFiles'   , function() {var dists = ['jqueryUmsOverlay/dist/*.js'];       return del(dists); } );
gulp.task('cleanJqueryUmsModalDistFiles'     , function() {var dists = ['jqueryUmsModal/dist/*.*'];          return del(dists); } );
gulp.task('cleanJqueryImageSliderDistFiles'  , function() {var dists = ['jqueryImageSlider/dist/*.js'];      return del(dists); } );
gulp.task('cleanJqueryBreakPointsDistFiles'  , function() {var dists = ['jqueryUmsBreakPoints/dist/*.js'];   return del(dists); } );
gulp.task('cleanJqueryScrollTriggerDistFiles', function() {var dists = ['jqueryUmsScrollTrigger/dist/*.js']; return del(dists); } );

gulp.task // jqueryUmsTextMeter
(
	'jqueryUmsTextMeter',
	function()
	{
		var dist = 'jqueryUmsTextMeter/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src('jqueryUmsTextMeter/src/jqueryUmsTextMeter.js')
			.pipe(beautify(beautify_options))
			.pipe(uglify())
			.pipe(concat('jqueryUmsTextMeter.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;

		gulp
			.src('jqueryUmsTextMeter/src/jqueryUmsTextMeter.js')
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;
	}
);

gulp.task // jqueryUmsOverlay
(
	'jqueryUmsOverlay',
	function()
	{
		var dist = 'jqueryUmsOverlay/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src('jqueryUmsOverlay/src/*.js')
			.pipe(beautify(beautify_options))
			.pipe(uglify())
			.pipe(concat('jqueryUmsOverlay.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;

		gulp
			.src('jqueryUmsOverlay/src/jqueryUmsOverlay.js')
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;
	}
);

gulp.task // jqueryScrollMonitor
(
	'jqueryScrollMonitor',
	function()
	{
		var dist = 'jqueryScrollMonitor/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src('jqueryScrollMonitor/src/*.js')
			.pipe(beautify(beautify_options))
			.pipe(uglify())
			.pipe(concat('jqueryScrollMonitor.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;

		gulp
			.src('jqueryScrollMonitor/src/jqueryScrollMonitor.js')
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;
	}
);

gulp.task // jqueryUmsBreakPoints
(
	'jqueryUmsBreakPoints',
	function()
	{
		var dist = 'jqueryUmsBreakPoints/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src('jqueryUmsBreakPoints/src/*.js')
			.pipe(beautify(beautify_options))
			.pipe(uglify())
			.pipe(concat('jqueryUmsBreakPoints.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;

		gulp
			.src('jqueryUmsBreakPoints/src/jqueryUmsBreakPoints.js')
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;
	}
);

gulp.task // jqueryUmsModal
(
	'jqueryUmsModal',
	function()
	{
		var dist = 'jqueryUmsModal/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src('jqueryUmsModal/src/*.js')
			.pipe(uglify())
			.pipe(concat('jqueryUmsModal.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;

		gulp
			.src('jqueryUmsModal/src/*.css')
			.pipe(cssbeautify())
			.pipe(minifyCss())
			.pipe(concat('jqueryUmsModal.min.css'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;
	}
);

gulp.task // jqueryUmsScrollTrigger - Sync FTP
(
	'jqueryUmsScrollTriggerSyncFtp',
	function()
	{
		var conn = ftp.create
		(
			{
				host     : '187.45.195.28',
				user     : 'umstudiohomolog',
				password : 'studio0001',
				parallel : 10,
				log      : gutil.log
			}
		);

		var globs =
		[
			'jqueryUmsScrollTrigger/**',
			'jqueryUmsScrollTrigger/Readme.md'
		];

		return gulp
			.src( globs, { base: '.', buffer: false } )
			.pipe( conn.newer( '/public_html/ums/jquery-plugins/' ) )
			.pipe( conn.dest( '/public_html/ums/jquery-plugins/' ) )
		;
	}
);

gulp.task // jqueryUmsScrollTrigger
(
	'jqueryUmsScrollTrigger',
	function()
	{
		var dist = 'jqueryUmsScrollTrigger/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src('jqueryUmsScrollTrigger/src/*.js')
			.pipe(beautify(beautify_options))
			.pipe(sourcemaps.init())
			.pipe(uglify())
			.pipe(concat('jqueryUmsScrollTrigger.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(sourcemaps.write('../maps'))
			.pipe(gulp.dest(dist))
		;
	}
);

gulp.task // jqueryUmsImageSlider
(
	'jqueryUmsImageSlider',
	function()
	{
		var dist = 'jqueryUmsImageSlider/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src('jqueryUmsImageSlider/src/*.js')
			.pipe(beautify(beautify_options))
			.pipe(sourcemaps.init())
			.pipe(uglify())
			.pipe(concat('jqueryUmsImageSlider.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(sourcemaps.write('../maps'))
			.pipe(gulp.dest(dist))
		;
	}
);

gulp.task // jqueryUmsMustache
(
	'jqueryUmsMustache',
	function()
	{
		var dist = 'jqueryUmsMustache/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src
			(
				[
					'jqueryUmsMustache/src/phpjs.js',
					'node_modules/mustache/mustache.min.js',
					'jqueryUmsMustache/src/ums_mustache.js'
				]
			)
			.pipe(replace('CJS_DEBUG_MODE_1', 'CJS_DEBUG_MODE_0'))
			.pipe(beautify(beautify_options))
			.pipe(concat('jqueryUmsMustache.js'))
			.pipe(gulp.dest(dist))
			.pipe(uglify())
			.pipe(concat('jqueryUmsMustache.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;
	}
);

gulp.task // jqueryUmsAutoSearch
(
	'jqueryUmsAutoSearch',
	function()
	{
		var dist = 'jqueryUmsAutoSearch/dist';
		var beautify_options = { 'indent_with_tabs': true, 'brace-style': 'expand' };

		gulp
			.src('jqueryUmsAutoSearch/src/jqueryUmsAutoSearch.js')
			.pipe(beautify(beautify_options))
			.pipe(uglify())
			.pipe(concat('jqueryUmsAutoSearch.min.js'))
			.pipe
			(
				edit
				(
					function(src, cb)
					{
						src = '// Last modified: ' + date_now + '\n' + src;
						cb(null, src);
					}
				)
			)
			.pipe(gulp.dest(dist))
		;
	}
);

// gulp.task // watch
// (
// 	'watch',
// 	function()
// 	{
// 		var wJqueryUmsOverlay       = ['jqueryUmsOverlay/src/*.js'      ,'jqueryUmsOverlay/src/*.css'      ];
// 		var wJqueryUmsModal         = ['jqueryUmsModal/src/*.js'        ,'jqueryUmsModal/src/*.css'        ];
// 		var wJqueryUmsImageSlider   = ['jqueryUmsImageSlider/src/*.js'  ,'jqueryUmsImageSlider/src/*.css'  ];
// 		var wJqueryUmsBreakPoints   = ['jqueryUmsBreakPoints/src/*.js'  ,'jqueryUmsBreakPoints/src/*.css'  ];
// 		var wJqueryUmsScrollTrigger = ['jqueryUmsScrollTrigger/src/*.js'];
// 		// var wJqueryUmsScrollTrigger2 = ['jqueryUmsScrollTrigger/demo/**'];

// 		gulp.watch(wJqueryUmsOverlay      , ['cleanJqueryUmsOverlayDistFiles'   , 'jqueryUmsOverlay']);
// 		gulp.watch(wJqueryUmsModal        , ['cleanJqueryUmsModalDistFiles'     , 'jqueryUmsModal']);
// 		gulp.watch(wJqueryUmsImageSlider  , ['cleanJqueryImageSliderDistFiles'  , 'jqueryUmsImageSlider']);
// 		gulp.watch(wJqueryUmsBreakPoints  , ['cleanJqueryBreakPointsDistFiles'  , 'jqueryUmsBreakPoints']);
// 		gulp.watch(wJqueryUmsScrollTrigger, ['cleanJqueryScrollTriggerDistFiles', 'jqueryUmsScrollTrigger']);
// 		// gulp.watch(wJqueryUmsScrollTrigger2, ['jqueryUmsScrollTriggerSyncFtp']);
// 	}
// );

// gulp.task('_default', ['watch']);